# StringOverflow
A tool for managing project knowledge


## Premise

There are tons of communication tools on the internet and they work really well.
StackOverflow, the place to ask questions
Twitter, The place to voice your information
Google, the place to find your knowldge

These are massive tools that we use everyday in our personal life, but not in project life.

In project life, we're stuck with wiki and Slack.
Great tools, but when was the last time you searched Wiki for the awnswer and found it?
Who on earth has the time to dive through Slack conversation to find that one bit of info?
Does anyone broadcast information on Slack, just for the sake of posting it somewhere?
and doesn't posting to slack targetlessly defeat the idea of Slack itself?

Wiki has it's own issue. It's stuck in stone, it's too static. 
It's great for instructions, how to setup a project etc. What is written there is the true.
But it's really an information dead-end.
Wiki articles are too formal and get outdated too fast for modern development.
Articles are just used as proof that something is written 'somewhere' and if you can't find it, it's your issue.

## Resolution

We know what works,
* what is the proportion of devs that check StackOverflow vs a project's Wiki? I bet 99%
* What is the proportion of devs that use the StackOverflow search vs google? I bet 99%
* What is easily the most useful political tool? (Projects are political) Twitter

These tools breakdown on projects as they have global scope,
so let use the best parts of these tools to make a project knowledge sharing tool that works.

## Screens

### Ask / Answer  a question
* one word hashtag
* simple box to ask a question
* answer question using the question - set expiry date ( make sure info doesn't go out of date )

#### Example

* Q: #react, how does one do a React?
* A: Read xxx.react.com

### Search / Enter
* one word hashtag
* enter a sentence regarding - set expiry date ( make sure info doesn't go out of date )
* read results

#### Example

* #buildScripts: buildscripts are not working - expires in 7 days

### Tech

* ElasticSearch
* React
