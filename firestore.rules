// read = list / get
// write = create / update
service cloud.firestore {
  match /databases/{database}/documents {
  	
    match /users {    	
      match /{userId} {
      	
        allow read: if true;
        allow create: if request.auth.uid == userId;
        allow update: if true;

        match /clients/{clientName} {
        	allow read, write;
        }

        match /requests/{clientName} {
          allow read, write: if request.auth.uid == userId; 
        }
      }
    }
    
    match /clients/{client} {
      
      function isSignedIn(){
        return request.auth.uid != null;
      }
      function signedInAndPublic() {
        return request.auth.uid != null || resource.data.public == true;
      }
      function isClientPublic(){
        return get(/databases/$(database)/documents/clients/$(client)).data.public == true;   
      }
      function isClientMember() {
        return exists (/databases/$(database)/documents/clients/$(client)/users/$(request.auth.uid));
      }
      function isClientAdmin() {
        return request.auth.uid in get(/databases/$(database)/documents/clients/$(client)).data.admins;   
      }
      function ownedByUser(){
        return request.auth.uid == resource.data.createdBy
      }

      // any logged in user can create a new project
      allow create: if isSignedIn();
      // allow anyone to list the projects
      allow list: if true;
      // if a project is public or if the user is a memeber, let it be read
      allow get: if isClientPublic() || isClientMember();

      // only admins can edit the root
      allow update, delete: if isClientAdmin();

      // boards
      match /boards/{boardName} {
        // if the client is public or the client is a member, let it be read 
        allow read: if isClientPublic() || isClientMember();
        // only admins can edit the board
        allow update, delete, create: if isClientAdmin();

        match /comments/{comment} {
          // if the client is public or the client is a member, let it be read 
          allow read: if isClientPublic() || isClientMember();
          // only memebers can move/delete tickets
          allow update, create, delete: if isClientMember();
        }
      }

      // comments
      match /comments/{comment} {
         // if the client is public or the client is a member, let it be read 
        allow read: if isClientPublic() || isClientMember();
        // only members can create comments
        allow create: if isClientMember();
        // only owners or admins can edit root comments
        allow update, delete: if isClientAdmin() || ownedByUser();

        
        // comment responses
        match /responses/{response} {
          // if the client is public or the client is a member, let it be read 
          allow read: if isClientPublic() || isClientMember();
          // only members can create responses
          allow create: if isClientMember();
          // only owners or admins can edit responses
          allow delete: if isClientAdmin() || ownedByUser();
        }
      }
      
      // topics
      match /topics/{topic} {
        // if the client is public or the client is a member, let it be read 
        allow read: if isClientPublic() || isClientMember();
        // only members can create responses
        allow create: if isClientMember();

        match /references/{reference} {
          // if the client is public or the client is a member, let it be read 
          allow read: if isClientPublic() || isClientMember();
          // only members can create responses
          allow create: if isClientMember();        
        }
      }
      
      // requests
      match /requests/{user} {
        // any user can request to join - creation is handled server side now.
        // allow create: if isSignedIn();
        // allow read, update: ownedByUser()
        allow read: if isClientPublic() || isClientMember();
        // allow delete if owned by the user, or is admin
        allow delete: if ownedByUser() || isClientAdmin();
      }
      
      // users
      match /users/{users} {
        // if the client is public or the client is a member, let it be read 
        allow read: if isClientPublic() || isClientMember();
        // only admins should be able to add userts
        allow write: if isClientAdmin();
        //
        allow delete: if ownedByUser() || isClientAdmin();
      }        
    }    
  }
}
