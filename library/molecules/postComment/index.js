import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { checkForHash, postComment, attachImage, uploadImage } from '../../../methods/comments'
import { makeTicket } from '../../../methods/board'
import styles from './styles.scss'
import { Images, Button, TagAutoComplete } from 'atoms'
import { CTA, FormRow } from 'molecules'
import Dropzone from 'react-dropzone'

class PostComment extends Component {
  constructor(props) {
    super()
    this.defaultState = props.prefixTag ? `${props.prefixTag} ` : '';
    this.state = {
      state: '',
      tags: this.defaultState,
      loading: false,
      files: false
    }
  }

  onDrop(files) {
    this.setState({ files })
  }

  async toBacklog() {
    const boardRef = this.props.backlogRef
    let commentRef = await this.submit()
    await makeTicket(boardRef, commentRef, true)
  }

  async submit() {
    let tags1 = checkForHash(this.state.state).map(tag => tag[1])
    let tags2 = this.state.tags.trim().split(' ')
    const tags = [...tags1, ...tags2]

    if (tags.length > 0) {
      this.setState({ loading: true })
      const clientId = this.props.match.params.clientId
      const commentsRef = this.props.db.collection('clients').doc(clientId).collection('comments')
      const userRef = this.props.db.collection('users').doc(this.props.user.uid)
      const commentRef = await postComment(commentsRef, userRef, this.state.state, tags)

      if (this.state.files) {
        let filePromises = this.state.files.map(file =>
          uploadImage(this.props.storage, clientId, commentRef.id, file)
        )
        let snapshots = await Promise.all(filePromises)
        let urls = snapshots.map(snapshot => snapshot.downloadURL)
        attachImage(commentRef, urls)
      }
      this.setState({ state: '', tags: '', files: false, loading: false })
      return commentRef;
    } else {
      alert('Must have 1 #tag')
    }
  }

  cancel() {

  }

  render() {
    return (
      <div>
        <h3>Post Comment</h3>
        <FormRow
          label="Text:"
          type="text"
          value={this.state.state}
          disabled={this.state.loading}
          onChange={e => { this.setState({ state: e.target.value }) }} />

        <Dropzone
          onDrop={this.onDrop.bind(this)}
          accept=".jpeg,.png,.jpg,.gif"
          multiple={true}
          className={styles.dropzone}
          activeClassName={styles.active}
          rejectClassName={styles.reject}
        >
          <Button>Drag / Attach an image</Button>
        </Dropzone>
        
        {this.state.files &&
          <div className={styles.imageWrapper}>
            <Button
              className={styles.imageClear}
              onClick={this.setState.bind(this, { files: false }, () => { })}
            >
              X
            </Button>
            <Images
              images={this.state.files.map(file => ({ src: file.preview }))}
            />
          </div>
        }

        {(this.state.state || this.state.tags) &&
          <FormRow
            disabled={this.state.loading}
            label="Tags:"
            type="tags"
            value={this.state.tags}
            onChange={tags => { this.setState({ tags }) }} />
        }

        {this.state.state && this.state.tags &&
          <CTA
            disabled={this.state.loading}
            onAgree={this.submit.bind(this)}
            onCancel={this.cancel.bind(this)}
            extraButtons={[['Add To Backlog', this.toBacklog.bind(this)]]}
          >
            Post
            </CTA>
        }
      </div>
    )
  }
}

export default withRouter(connect(state => state)(PostComment))