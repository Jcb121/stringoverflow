import React, { Component } from 'react'
import styles from './styles.scss'
import { TagAutoComplete } from 'atoms'

export class FormRow extends Component {
  render(){
    return (
      <label className={styles.wrapper}>
        <div className={styles.label}>
          {this.props.label}
        </div>

        <div className={styles.inputWrapper}>

          {this.props.type === 'tags' ?
            <TagAutoComplete {...this.props}
              inputProps={{className: styles.input}}
            />
          :
            <input disabled={this.props.disabled}
            onChange={this.props.onChange}
            className={styles.input}
            value={this.props.value}
          />
          }          

        </div>
      </label>
    )
  }
}

