import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkForHash, postQuestion } from '../../../methods/comments'
import { withRouter } from 'react-router';
import styles from './styles.scss'
import { Button } from 'atoms'
import { FormRow } from 'molecules'
import { CTA } from 'molecules'

// There are a few implementations of this question
// quick as, formal ask.

class PostQuestion extends Component {
  constructor(props) {
    super(props)
    // Get URL props from props.history
    this.defaultTags = props.prefixTag ? `${props.prefixTag} ` : '';
    this.state = {
      state: '',
      tags: this.defaultTags,
      loading: false
    }
  }

  handleQuestionChange(e) {
    this.setState({ state: e.target.value })
  }

  handleTagChange(tags) {
    this.setState({ tags })
  }

  onBlur() {
    // this.props.history.push(`/ask/${this.state.state}`)
  }

  async submit() {
    let tags1 = checkForHash(this.state.state).map(tag => tag[1])
    let tags2 = this.state.tags.trim().split(' ')
    const tags = [...tags1, ...tags2]

    if (tags.length > 0) {
      this.setState({ loading: true })

      const clientId = this.props.match.params.clientId
      const commentsRef = this.props.db
        .collection('clients').doc(clientId)
        .collection('comments') 
      const userRef = this.props.db
        .collection('users').doc(this.props.user.uid)

      const commentRef = await postQuestion(
        commentsRef,
        userRef,
        this.state.state,
        tags
      )
      
      this.setState({ state: '', tags: '', loading: false })
      return commentRef;
    } else {
      alert('Must have 1 #tag')
    }
  }

  cancel() {

  }

  render() {
    return (
      <div>
        <FormRow
          disabled={this.state.loading}
          label="Ask question:"
          onBlur={this.onBlur.bind(this)}
          type="text"
          value={this.state.state}
          onChange={this.handleQuestionChange.bind(this)} />

        {(this.state.state || this.state.tags) &&
          <FormRow
            disabled={this.state.loading}
            label="Tags:"
            type="tags"
            value={this.state.tags}
            onChange={this.handleTagChange.bind(this)} />
        }

        {(this.state.state || this.state.tags) &&
          <div className={styles.cta}>
            <CTA
              disabled={this.state.loading}
              onAgree={this.submit.bind(this)}
              onCancel={this.cancel.bind(this)}>
              Post
            </CTA>
          </div>
        }

      </div>
    )
  }
}

export default withRouter(connect(state => state)(PostQuestion))
