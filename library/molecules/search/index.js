import React, { Component } from 'react'
import styles from './styles.scss'
import Autocomplete from 'react-autocomplete'
import { withRouter } from 'react-router'
import { CollectionId } from 'react-firestore'

class Search extends CollectionId {
  constructor(props) {
    super()
    this.state = {
      state: '',
      documents: []
    }
  }
  
  handleChange(e) {
    this.setState({ state: e.target.value })
  }

  submit() {
    alert('Submit Search!')
  }

  render() {
    const { clientId } = this.props.match.params
    return (
      <Autocomplete
        inputProps={{
          className: styles.input,
          placeholder: 'Seach'
        }}
        wrapperStyle={{display: 'grid'}}                
        getItemValue={(item) => item }
        onChange={this.handleChange.bind(this)}          
        onSelect={(topic)=> {
          this.props.history.push(`/clients/${clientId}/topics/${topic}`)
        }}
        value={this.state.state}
        renderItem={(item, isHighlighted) => {
          return <div 
            key={item}
            className={isHighlighted ? styles.highLighted : ''}
          >
            &nbsp;{item}
          </div>
        }}
        items={this.state.documents}/>
    )
  }
}

export default withRouter(Search)