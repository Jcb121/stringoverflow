import React, { Component } from 'react'
import styles from './styles.scss'
import { Button, Card } from 'atoms'

export class FloatingCard extends Component {
  constructor(props){
    super(props)
    this.state = {
      open: false
    }
  }
  openClick() {
    if (!this.state.open) {
      document.addEventListener('click', this.handleOutsideClick.bind(this), false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    }
    this.setState(prevState => ({ open: !prevState.open }))
  }
  handleOutsideClick(e) {
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    // this will open other 
    document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    this.setState(prevState => ({ open: false }))
  }
  render(){
    return (
      <div 
        ref={node => { this.node = node }}
        className={styles.root}>
        <Button
          className={this.props.buttonClass}
          onClick={this.openClick.bind(this)}
        >
          {this.props.label}
        </Button>
        {this.state.open &&
          <Card className={`${styles.wrapper} ${this.props.className}`}>
            {this.props.children}
          </Card>
        }
      </div>
    )
  }
}

export class Floating extends Component {
  constructor(props){
    super(props)
    this.state = {
      open: false
    }
  }
  openClick() {
    if (!this.state.open) {
      document.addEventListener('click', this.handleOutsideClick.bind(this), false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    }
    this.setState(prevState => ({ open: !prevState.open }))
  }
  handleOutsideClick(e) {
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    // this will open other 
    document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    this.setState(prevState => ({ open: false }))
  }
  render(){
    const child = React.Children.only(this.props.children)
    return (
      <div 
        ref={node => { this.node = node }}
        className={styles.root}>
        <Button
          className={this.props.buttonClass}
          onClick={this.openClick.bind(this)}
        >
          {this.props.label}
        </Button>
        {this.state.open && child }
      </div>
    )
  }
}