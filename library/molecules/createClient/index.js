import React, { Component } from  'react'
import { FormRow, CTA } from 'molecules'
import { createClient } from '../../../methods/client'

export class CreateClient extends Component {
  constructor(){
    super();
    this.state = {
      state: '',
      loading: false
    }
  }
  handleClientChange(e) {
    this.setState({ state: e.target.value.replace(/ /g, '')})
  }
  render(){
    return (
      <div>
        <FormRow
          value={this.state.state} 
          type="text"
          onChange={this.handleClientChange.bind(this)}/>

        <CTA 
          disabled={this.state.loading} 
          onAgree={this.addNewClient.bind(this)}>
          Create
        </CTA>
      </div>
    )
  }

  async addNewClient() {
    this.setState({ loading: true })
    const clientsRef = this.props.db.collection('clients')
    createClient(clientsRef, this.state.state, this.props.user.uid).then(clientRef =>{
      this.setState({
        state: '',
        loading: false
      })
    }).catch(error => {
      alert(`Client name is unavaible ${this.state.state}`)
      console.log(error)
      this.setState({ loading: false })
    })
  }
}