import React, { Component } from 'react'

export class PostAnswer extends Component {
  constructor() {
    super()
    this.state = {
      state: ''
    }
  }

  handleChange(e) {
    this.setState({ state: e.target.value })
  }

  submit() {
    alert('Submit Answer!')
  }
  
  render() {
    return (
      <div>
        Answer!
          <input
          type="text"
          value={this.state.state}
          onChange={this.handleChange.bind(this)} />
        <button onClick={this.submit.bind(this)}>
          Answer!
          </button>
      </div>
    )
  }
}