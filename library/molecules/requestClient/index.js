import React, { Component } from  'react'
import {FormRow} from '../formrow'
import {CTA} from '../cta'

export default class RequestClient extends Component {
  constructor(){
    super();
    this.state = {
      state: '',
      loading: false
    }
  }
  handleClientChange(e) {
    this.setState({ state: e.target.value.replace(/ /g, '')})
  }
  render(){
    return(
      <div>
        <FormRow
          label="Request access to:"
          type="text"
          value={this.state.state}
          onChange={this.handleClientChange.bind(this)}/>        

        <CTA disabled={this.state.loading}
          onAgree={this.requestClient.bind(this)}>
          Request
        </CTA>
    </div>
    )
  }
  
  requestClient(){
    this.setState({ loading: true })
    const userRef = this.props.db.collection('users').doc(this.props.user.uid)
    const clientName = this.state.state
    requestClient(userRef, clientName)
      .then(() => {
        this.setState({
          loading: false,
          state: '' 
        })
      }).catch(error => {
        this.setState({ loading: false })
        console.log(error)
      })
  }
}
