import React from 'react'
import styles from './styles.scss'
import { CollectionRef } from 'react-firestore'
import { Link } from 'atoms'
import { AccessRequests } from 'molecules'
import { connect } from 'react-redux'

class Clients extends CollectionRef {
  render() {
    return (
      <div className={styles.root}>
        {this.state.documents.map(clientRef => {
          let requests = this.props.db
            .collection('clients').doc(clientRef.id)
            .collection('requests')
          return (
            <div key={clientRef.id} className={`${styles.client} ${this.props.className}`}>
              <Link to={`/clients/${clientRef.id}`}>
                {clientRef.id}
              </Link>
              <AccessRequests
                className={this.props.requestClassName}
                collection={requests}
              />
              {typeof this.props.children === 'function' ?
                this.props.children(clientRef)
                :
                this.props.children
              }
            </div>
          )
        })}
      </div>
    )
  }
}

export default connect(state => state)(Clients)