import React, { Component } from 'react'
import styles from './styles.scss'
import { Button } from 'atoms'

export class CTA extends Component {
  render() {
    return (
      <div className={styles.wrapper}>
        <Button disabled={this.props.disabled}
          onClick={this.props.onCancel}>Cancel</Button>
        <Button disabled={this.props.disabled}
          onClick={this.props.onAgree}>{this.props.children}</Button>
        {this.props.extraButtons && this.props.extraButtons.map(([text, func]) =>
          <Button key={text} onClick={func}>{text}</Button>
        )}
      </div>
    )
  }
}

