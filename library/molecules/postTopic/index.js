import styles from './styles.scss'
import React, { Component } from 'react'
import { FormRow } from 'molecules'
import { CTA } from 'molecules'
import { withRouter } from 'react-router-dom' 

class PostTopic extends Component {
  constructor(){
    super()
    this.state = {
      state: '',
      loading: false
    }
  }

  handleChange(e) {
    this.setState({ state: e.target.value.replace(/ /g, '')})
  }

  cancel(){
    this.setState({
      state: '',
      loading: false
    })
  }

  async submit(){
    this.setState({loading: true})
    const topicsRef = this.props.db
      .collection('clients').doc(this.props.match.params.clientId)
      .collection('topics')
    const userRef = this.props.db.collection('users').doc(this.props.user.uid)
    const topicRef = await postComment(topicsRef, userRef, this.state.state)  
    this.setState({state: '', loading: false})
  }

  render(){
    return(
      <div className={styles.root}>
        <h2>New Topic</h2>

        <FormRow 
          label="Title:"
          type="text"
          value={this.state.state}
          onChange={this.handleChange.bind(this)} />

        <CTA
          disabled={this.state.loading}
          onAgree={this.submit.bind(this)}
          onCancel={this.cancel.bind(this)}>
          Post
        </CTA>
      </div>
    )
  }
}

export default withRouter(PostTopic)