import { AccessRequests } from './accessRequests'
import { CreateClient } from './createClient'
import { CTA } from './cta'
import { FormRow } from './formrow'
import { PostAnswer } from './postAnswer'
import PostComment from './postComment' // with router
import PostQuestion from './postQuestion' // with router
import PostTopic from './postTopic'// with router
import RequestClient from './requestClient'
import Response from './response'
import Search from './search'
import Topic from './topic' // with router
import Clients from './clients'
import Topics from './topics' // with router
import { FloatingCard, Floating } from './floatingCard'
import Tasks from './tasks'

export {
    AccessRequests,
    CreateClient,
    CTA,
    FormRow,
    PostAnswer,
    PostComment,
    PostQuestion,
    PostTopic,
    RequestClient,
    Response,
    Search,
    Topic,
    Clients,
    FloatingCard,
    Floating,
    Topics,
    Tasks
}
