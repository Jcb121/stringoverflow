import React, { Component } from 'react';
import styles from './styles.scss'
import { CollectionRef } from 'react-firestore'
import { PostTags } from 'atoms'

export default class Topics extends CollectionRef {
  render() {
    let tags = this.state.documents.map(topic => topic.id) 
    return (
      <PostTags className={this.props.className} 
        tags={tags} 
        clientId={this.props.collection.parent.id}/>
    )
  }
}