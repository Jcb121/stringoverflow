import React from 'react'
import { connect } from 'react-redux'
import { CollectionRef, Document } from 'react-firestore'
import { Title, Button } from 'atoms'
import styles from './styles.scss'

class Timer extends Document {
  constructor(){
    super()
    this.state = {
      commentRef: false
    }
  }
  startTracking() {  
    this.props.document.update({
      startedAt: Date.now()
    })
  }
  
  stopTracking() {
    let elapsedTime = this.state.elapsedTime ? this.state.elapsedTime : 0
    elapsedTime = elapsedTime + (Date.now() - this.state.startedAt)

    this.props.document.update({
      elapsedTime,
      startedAt: false
    })
  }
  render(){
    return (
      <div className={styles.timer__root}>
        { this.state.commentRef &&
          <Title type="p" document={this.state.commentRef}/>
        }
        
        <div>
          time elapsed: {this.state.elapsedTime}
        </div>
        
        {this.state.startedAt && 
          <Button onClick={this.stopTracking.bind(this)}> 
            Stop Tracking
          </Button>
        }

        {typeof this.state.elapsedTime === 'undefined' && !this.state.startedAt && 
          <Button onClick={this.startTracking.bind(this)}>
            Start Tracking
          </Button>
        }
        
        {this.state.elapsedTime && !this.state.startedAt &&
          <Button onClick={this.startTracking.bind(this)}>
            Continue Tracking
          </Button>
        }

      </div>
    )
  }
}

class TasksCollection extends CollectionRef {
  render(){  
    return (
      <div>
        {this.state.documents.map(ticketRef => {
          return (
            <Timer key={ticketRef.id} document={ticketRef} />
          )
        })}
      </div>
    )
  }
}

class Tasks extends CollectionRef {
  render(){
    return (
      <div>
        {this.state.documents.map(boardRef => {
          const ticketsRef = boardRef.collection('comments')

          return (
            <div key={boardRef.id}>
              <TasksCollection 
                collection={ticketsRef}
                where={['userRef', '==', this.props.user.ref]} 
                key={boardRef.id} 
              />
            </div>
          ) 
        })}
      </div>
    )
  }
}

export default connect(state => state)(Tasks)