import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Comment } from 'organisms'
import styles from './styles.scss'
import { CollectionRef } from 'react-firestore'

class Topic extends CollectionRef {
  render() {
    return (
      <div>
        <ol className={styles.listWrapper}>
          {this.state.documents.map(ref => {
            return (
              <li 
                key={`topic_${ref.id}`}
                className={styles.li}
                >
                <Comment
                  document={ref}
                  level={0}
                  showChildren={true}
                  showImages={true}
                />
              </li>
            )
          })}
        </ol>
      </div>
    )
  }
}

export default withRouter(Topic)