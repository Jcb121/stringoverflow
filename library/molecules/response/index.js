import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { withRouter } from 'react-router';
import { Document } from 'react-firestore'
import { makeTicket } from '../../../methods/board'
import { updateCommentText, solveQuestion } from '../../../methods/comments'
import {
  Link,
  Button,
  PostTags,
  Avatar,
  Images
} from 'atoms'

class Response extends Document {
  constructor(props) {
    super(props)
    this.state = {
      state: '',
      rating: 0,
      data: '',
      responding: props.location.search.indexOf(`responding=${this.props.document.id}`) > -1,
      editing: false,
      solved: false,
      tags: [],
      images: []
    }
  }
  componentDidMount() {
    if (this.state.responding) {
      this.refs.response.focus()
    }
  }
  async respond() {
    let ref = await this.props.addResponse(this.props.document, this.state.state)
    this.setState({ state: '', responding: false })
    return ref
  }
  async makeTicket() {
    let commentRef = await this.respond()
    let boardRef = this.state.boardRef
    makeTicket(boardRef, commentRef)
  }
  render() {
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false
    let photoRef = this.state.userRef
    if (this.props.boardRef) {
      photoRef = this.props.userRef ? this.props.userRef : false
    }
    return (
      <div className={`
        ${styles.root}
        ${this.state.editing ? styles.editing : ''}
        ${this.state.resolved ? styles.resolved : ''}
        ${this.state.question ? styles.question : ''}
      `}>

        <div className={styles.wrapper}>
          {photoRef &&
            <Avatar document={photoRef} className={styles.avatar} />
          }
          {this.state.editing
            ? <div>
              <input type="text" value={this.state.data}
                onChange={e => { this.setState({ data: e.target.value }) }} />
              <button onClick={() => {
                this.setState({ editing: false })
                updateCommentText(this.props.document, this.state.data)
              }}>Save</button>
            </div>
            : <span >

              {this.props.areLinks ?
                <Link
                  to={`/clients/${this.props.match.params.clientId}/comments/${this.props.document.id}`}>
                  {this.state.data}
                </Link>
                :
                <p>
                  {this.state.data}
                </p>
              }

            </span>
          }
        </div>

        {this.props.showImages &&
          <Images
            className={styles.images}
            images={this.state.images.map(src => ({ src }))}
          />
        }

        <PostTags tags={this.state.tags} clientId={this.props.document.parent.parent.id} />

        {this.props.user.uid &&
          <div className={styles.actions}>
            {this.state.question && this.state.resolved === false &&
              <button className={styles.action} onClick={() => {
                solveQuestion(this.props.document)
              }}>Mark Solved</button>
            }
            
            {loggedIn && isMember && typeof this.props.addResponse !== 'undefined' &&
              <button className={styles.action}
                onClick={this.setState.bind(this, { responding: true }, null)}>
                Reply
              </button>
            }
            
            {loggedIn && isMember && 
              <button className={styles.action}
                onClick={this.setState.bind(this, { editing: true }, null)}>
                Edit
              </button>
            }

            {this.props.children && React.Children.map(this.props.children, child => {
              if (child === null || typeof child.props.onClick === 'undefined') {
                return child;
              }
              return <button
                {...child.props}
                className={`${child.props.className} ${styles.action}`}
                onClick={child.props.onClick.bind(this)}>
                {child.props.children}
              </button>
            })}
          </div>
        }

        {this.state.responding && <div className={styles.response}>
          <textarea
            className={styles.response_input}
            ref="response"
            type="text"
            value={this.state.state} onChange={e => {
              this.setState({ state: e.target.value })
            }} />
          <div className={styles.response_actions}>
            <Button onClick={this.makeTicket.bind(this)}>
              Make Ticket
            </Button>

            <Button onClick={this.respond.bind(this)}>
              Post
            </Button>

            <Button onClick={this.setState.bind(this, { state: '', responding: false }, null)}>
              Cancel
            </Button>
          </div>

        </div>
        }
      </div>
    )
  }
}

export default withRouter(connect(state => state)(Response))