import React, { Component } from 'react'
import { CollectionId, Document } from 'react-firestore'
import { Button } from 'atoms'
import styles from './styles.scss'

class User extends Document {
  constructor(){
    super()
    this.state = {displayName: ''}
  }
  render(){
    return (
      <span>
        { this.state.displayName }
      </span>
    )
  }
}

export class AccessRequests extends CollectionId {

  delete(id){
    const requestsRef = this.props.collection
    return requestsRef.doc(id).delete()
  }

  approve(id){
    const clientRef = this.props.collection.parent
    const userRef = this.props.db.collection('users').doc(id)
    approveUser(clientRef, userRef)
  }

  render(){
    return(
      <ul className={`${styles.root} ${this.props.className}`}>
        {this.state.documents.map(id => {
          return (
            <li key={id} className={styles.wrapper}>
              <User document={this.props.db.collection('users').doc(id)}/>
              <div>
                <Button onClick={this.delete.bind(this, id)}>Delete</Button>
                <Button onClick={this.approve.bind(this, id)}>Approve</Button>
              </div>
            </li>
          )
        })}
      </ul>
    )
  }
}
