import React, { Component } from 'react'
import styles from './styles.scss'

export default class Footer extends Component {
  render(){
    return (
      <div className={styles.root}>
        © Jesse Baker 2018
      </div>
    )
  }
}