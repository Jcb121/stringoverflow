import React, { Component } from 'react'
import styles from './styles.scss'
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom' 
import { CollectionId } from 'react-firestore'

class ClientSelect extends CollectionId {
  handleChange(e){
    this.props.history.push(`/clients/${e.target.value}`)
  }

  render(){
    return (
      <select {...this.props.selectProps} 
        value={this.props.match.params.clientId} 
        onChange={this.handleChange.bind(this)}>
        {this.state.documents.map(client => {
          return (
            <option
              {...this.props.optionProps} 
              key={`client_select_${client}`} 
              value={client}>
              {client}
            </option>
          )
        })}
      </select>
    )
  }
}

export default withRouter(ClientSelect)