import React, { Component } from 'react'
import styles from './styles.scss'
import { Link as DomLink } from 'react-router-dom'

export class Button extends Component {
  render(){
    return (
      <button 
        onClick={this.props.onClick}
        className={`${styles.button} ${this.props.className}`}>
        {this.props.children}
      </button>
    )
  }
}

export class LinkButton extends Component {
  render(){
    return (
      <DomLink
        to={this.props.to}
        className={`${styles.button} ${this.props.className}`}>
        {this.props.children}
      </DomLink>
    )
  }
}

export class Link extends Component {
  render(){
    return (
      <DomLink
        to={this.props.to} 
        className={`${styles.link} ${this.props.className}`}>
        {this.props.children}
      </DomLink>
    )
  }
}