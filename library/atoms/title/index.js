import React, { Component } from 'react';
import { Document } from 'react-firestore'
import styles from './styles.scss'

export class Title extends Document {
  constructor(props){
    super(props)
    this.state = {
      data: ''
    }
  }
  render(){
    if(this.state.data){
      return (
        <this.props.type {...this.props}>
          {this.props.before}{this.state.data}{this.props.after} 
        </this.props.type>
      )
    } else {
      return (
        <this.props.type {...this.props} className={`${this.props.className} ${styles.loading}`}>
          &nbsp; 
        </this.props.type>
      )
    }
  }
}