import styles from './styles.scss'
import React, { Component } from 'react'
import { Link } from 'atoms'

export default class PostTags extends Component {
  render (){
    return(
      <ul className={`${styles.list} ${this.props.className}`}>
        {this.props.tags.map(tag => {
         return (
           <li className={styles.item} key={tag}>
              <Link className={styles.link} to={`/clients/${this.props.clientId}/topics/${tag}`}>
                {tag}
              </Link>
           </li>
         ) 
        })} 
      </ul>
    )
  }
}