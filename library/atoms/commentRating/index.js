import React, { Component } from 'react'
import styles from './styles.scss'

export class StaticCommentRating extends Component {
  render() {
    return (
      <div className={styles.box}>
        <div>{this.props.rating}</div>
        <div className={styles.label}>rating</div>
      </div>
    )
  }
}

export class ActionCommentRating extends Component {
  render() {
    return (
      <div className={styles.rating}>
        <button onClick={this.props.voteUp} className={styles.up}>▲</button>
        <span>{this.props.rating}</span>
        <button onClick={this.props.voteDown} className={styles.down}>▼</button>
      </div>
    )
  }
}