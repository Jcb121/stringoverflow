import React, { Component } from 'react'
import styles from './styles.scss'
import { isMobile } from 'react-device-detect'

export class Card extends Component {
  render(){
    return (
      <div className={`
        ${styles.root}
        ${this.props.className}
        ${isMobile ? styles.mobile : styles.desktop}  
      `}>
        {this.props.children}
      </div>
    )
  }
}