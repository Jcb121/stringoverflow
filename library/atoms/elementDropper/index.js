import React, { Component } from 'react'
import styles from './styles.scss'

export default class ElementDropper extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showing: false
    }
  }
  onClick() {
    if (!this.state.showing) {
      document.addEventListener('click', this.handleOutsideClick.bind(this), false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    }
    this.setState(prevState => ({ showing: !prevState.showing }))
  }
  handleOutsideClick(e) {

    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    // this will open other 
    document.removeEventListener('click', this.handleOutsideClick.bind(this), false);
    this.setState(prevState => ({ showing: false }))

  }
  render() {
    return (
      <div
        {...this.props}
        onClick={null}
        ref={node => { this.node = node; }}
        className={styles.root + ' ' + this.props.className}>
        <div onClick={this.onClick.bind(this)}>
          {this.props.children}
        </div>
        {this.state.showing &&
          <div className={styles.wrapper}>
            {this.props.options.map(option =>
              <div
                key={option}
                className={styles.element}
                onClick={e => {
                  this.onClick()
                  this.props.onClick(option)
                }}
              >
                {option}
              </div>
            )}
          </div>
        }
      </div>
    )
  }
}
