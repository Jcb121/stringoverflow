import { Document } from 'react-firestore'
import React from 'react'
import styles from './styles.scss'

export default class Avatar extends Document {
  constructor(props){
    super(props)
    this.state = {
      userPhoto: false,
      displayName: ''
    }
  }
  render() {
    return (
      <div className={`${styles.root} ${this.props.className}`}>
        {this.state.userPhoto ?  
          <img className={styles.img} src={this.state.userPhoto} />
          :
          <span>
            {this.state.displayName[0]}
          </span>
        }
  
      </div>
    )
  }
}