import React, { Component } from 'react'
import { connect } from 'react-redux'
import Autocomplete from 'react-autocomplete'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import styles from './styles.scss'

// THIS SHOULDN'T HAVE A REF TO ROUTE.

class TagAutoComplete extends Component {
  constructor(props){
    super()
    this.state = {
      state: props.value,
      items: [],
    }
  }
  componentDidMount(){
    this.unsubscribe = this.props.db.collection('clients').doc(this.props.match.params.clientId).collection('topics').onSnapshot(refs => {
      this.setState({
        items: refs.docs.map(doc => doc.id)
      })
    }, err => {
      console.log(this, err)
    })
  }

  handleItemSelect(topic) {
    this.setState({
      state: this.state.state + `${topic} `,
      items: this.state.items.filter(item => item !== topic)
    }, () => this.props.onChange(this.state.state))
  }

  handleChange(e) {
    this.setState(
      { state: e.target.value },
      () => this.props.onChange(this.state.state)
    )
  }

  componentWillUnmount(){
    this.unsubscribe()
  }

  render(){
    return (
      <Autocomplete
        {...this.props}
        items={this.state.items}        
        value={this.state.state}
        wrapperStyle={{display: 'grid'}}
        getItemValue={(item) => item }
        onChange={this.handleChange.bind(this)}
        onSelect={this.handleItemSelect.bind(this)}
        renderItem={(item, isHighlighted) => {
          return <div 
            key={item}
            className={isHighlighted ? styles.highLighted : ''}
          >
            &nbsp;{item}
          </div>
        }}
      />  
    )
  }
}

export default withRouter(connect(state => state)(TagAutoComplete))