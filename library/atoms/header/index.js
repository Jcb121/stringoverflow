import React, { Component } from 'react'
import styles from './styles.scss'
import { Link, Card } from 'atoms'
import { isMobile } from 'react-device-detect'
import { Floating } from 'molecules'

export default class Header extends Component {
  closeMenu(){
    this.menu.openClick()
  }
  render(){
    return (
      <Card className={`
        ${styles.root} 
        ${isMobile ? styles.mobile : styles.browser}
      `}>
        <Link className={styles.header_link} to={this.props.to}>
          <h1 className={styles.header}>
            Overflow
          </h1>
        </Link>

        {isMobile ?
          <Floating
            ref={node => this.menu = node}
            buttonClass={styles.header_button}
            label="☰"
          >
            <Card className={styles.header_menu}>
              <button className={styles.header_close} onClick={this.closeMenu.bind(this)}>
                ❌ 
              </button>
              {this.props.children}
            </Card>
          </Floating>
          :
          <div className={this.props.className}>
            {this.props.children}
          </div>
        }

      </Card>
    )
  }
}