import Lightbox from 'react-images';
import React, { Component } from 'react'
import styles from './styles.scss'

export class Image extends Component {
  constructor(){
    super()
    this.state = {
      landscape: false
    }
  }
  resize(){
    this.setState({
      landscape: this.img.naturalHeight < this.img.naturalWidth
    })
  }
  render() {
    return (
      <div className={styles.imageRoot}>
        <div
          className={`
            ${this.props.className} 
            ${styles.imageWrapper}
          `}
        >
          <img
            onLoad={this.resize.bind(this)}
            className={`
              ${this.state.landscape ? styles.landscape : styles.portrait}
              ${styles.image}
            `}
            ref={img => this.img = img}
            {...this.props}
          />
        </div>
      </div>
    )
  }
}

export class Images extends Component {
  constructor() {
    super()
    this.state = {
      lightboxIsOpen: false
    }
  }
  openLightbox(index, event) {
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  gotoImage(index) {
    this.setState({
      currentImage: index,
    });
  }
  handleClickImage() {
    if (this.state.currentImage === this.props.images.length - 1) return;

    this.gotoNext();
  }
  render() {
    return (
      <div className={`
        ${styles.root}
        ${this.props.className}
      `}>
        {this.props.images.map((file, index) =>
          <Image
            key={index}
            src={file.src}
            onClick={(e) => this.openLightbox(index, e)}
          />
        )}
        <Lightbox
          currentImage={this.state.currentImage}
          images={this.props.images}
          isOpen={this.state.lightboxIsOpen}
          onClickImage={this.handleClickImage.bind(this)}
          onClickNext={this.gotoNext.bind(this)}
          onClickPrev={this.gotoPrevious.bind(this)}
          onClickThumbnail={this.gotoImage.bind(this)}
          onClose={this.closeLightbox.bind(this)}
          preventScroll={true}
          showThumbnails={this.props.images.length > 1}
        />
      </div>
    )
  }
}
