import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Response } from 'molecules'
import { Responses } from 'organisms'
import { Document } from 'react-firestore'
import { Avatar, Link, LinkButton, PostTags } from 'atoms'
import { withRouter } from 'react-router-dom'

class Question extends Document {
  constructor(props) {
    super(props)
    this.state = {
      data: '',
      tags: []
    }
  }

  render() {
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false
    let color = this.props.level % 2 ? styles.odd : styles.even;
    return (
      <div className={`${styles.root} ${color}`}>
        <div>
          <div className={styles.header}>
            {this.state.userRef && 
              <Avatar document={this.state.userRef} className={styles.avatar} />
            }
            <Link className={styles.title}
            to={`/clients/${this.props.match.params.clientId}/questions/${this.props.document.id}`}>
              {this.state.data}
            </Link>
          </div>
          <PostTags tags={this.state.tags} clientId={this.props.document.parent.parent.id}/>
        </div>
        <div>  
          {loggedIn && isMember &&
            <LinkButton
              to={`/clients/${this.props.match.params.clientId}/questions/${this.props.document.id}?responding=${this.props.document.id}`}>                      
              Respond
            </LinkButton>
          }
        </div>
      </div>
    )
  }
}

export default withRouter(withRouter(connect(state => state)(Question)))