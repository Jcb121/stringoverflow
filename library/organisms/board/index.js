import { Document } from 'react-firestore'
import React from 'react'
import Comments from '../comments'
import styles from './styles.scss'
import { Droppable } from 'react-beautiful-dnd'
import Ticket from '../ticket'
import { isMobile } from 'react-device-detect'
import { Title, Card, Link } from 'atoms'
import { FormRow, CTA, FloatingCard } from 'molecules'
import { updateColumns } from '../../../methods/board'

export default class Board extends Document {
  constructor(props) {
    super(props)
    this.state = {
      columns: [],
      state: ''
    }
  }
  addNewColumn() {
    if (this.state.columns.indexOf(this.state.state) > -1) {
      alert('column already present')
      return
    }
    let columns = [...this.state.columns, this.state.state]
    updateColumns(this.props.document, columns)

    this.floatingCard.setState({
      open: false
    })
    this.setState({
      columns,
      state: '',
      addingMore: false
    })
  }
  cancelNewColumn() {
    this.floatingCard.setState({
      open: false
    })
    this.setState({
      state: ''
    })
  }
  handleNewColumnChange(e) {
    this.setState({ state: e.target.value })
  }
  render() {
    if (this.state.columns.length === 0) return null

    let commentId = this.props.document.id
    let commentRef = this.props.document.parent.parent.collection('comments').doc(commentId)
    let clientId = this.props.document.parent.parent.id
    return <div className={styles.root}>
      <div className={styles.header}>
        <Link to={`/clients/${clientId}/boards/${this.props.document.id}`}>
          <Title type="h1" className={styles.title} document={commentRef} after=" - Board" />
        </Link>

        {this.state.columns.length < 4 &&
          <FloatingCard
            label="+"
            buttonClass={styles.addColumn_button}
            ref={ref => this.floatingCard = ref}
          >
            <FormRow
              label="new column name:"
              value={this.state.state}
              type="text"
              onChange={this.handleNewColumnChange.bind(this)}>
            </FormRow>
            <CTA
              onAgree={this.addNewColumn.bind(this)}
              onCancel={this.cancelNewColumn.bind(this)}
            >
              Add
            </CTA>
          </FloatingCard>
        }

      </div>

      <div className={styles.board}>
        {this.state.columns.map(targetColumn => {
          return <div key={targetColumn} className={styles.wrapper}>
            <h3>
              {targetColumn}
            </h3>
            <Droppable
              droppableId={`${this.props.document.id}|${targetColumn}`}
              isDropDisabled={isMobile}
            >
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  className={`
                    ${styles.column}
                    ${snapshot.isDraggingOver ? styles.column_dragOver : ''}
                  `}
                  {...provided.droppableProps}
                >
                  {this.state[targetColumn + '_order'] && this.state[targetColumn + '_order'].map((commentId, index) => {
                    let ticketRef = this.props.document.collection('comments').doc(commentId)
                    return (
                      <Ticket
                        key={commentId}
                        index={index}
                        areLinks={this.props.areLinks}
                        document={ticketRef} />
                    )
                  })}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        })}

      </div>
    </div>
  }
}