import React, { Component } from 'react'
import Comment from '../comment'
import { CollectionRef } from 'react-firestore'
import { Button } from 'atoms'
import styles from './styles.scss'

export default class Comments extends CollectionRef {
  constructor(props) {
    super(props)
    this.state = {
      documents: []
    }
  }
  render() {
    return (
      <div>
        {this.state.documents.map(comment => {
          return <Comment
            className={styles.comment}
            backlogRef={this.props.backlogRef}
            showChildren={this.props.showChildren}
            showImages={this.props.showImages}
            level="0"
            areLinks={true}
            document={comment}
            key={comment.id} >
            {this.props.children}
          </Comment>
        })}
        <Button onClick={this.loadNext}>
          load more
        </Button>
      </div>
    )
  }
}