import React from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Response } from 'molecules'
import { Draggable } from 'react-beautiful-dnd'
import { isMobile } from 'react-device-detect'
import { deleteTicket } from '../../../methods/board'
import { Document } from 'react-firestore' 

function _deleteTicket(){
  const boardRef = this.props.boardRef
  const commentRef = this.props.document
  return deleteTicket(boardRef, commentRef)
}
function assignMe(){
  // this is bound to the response.  
  const userId = this.props.user.uid
  const userRef = this.props.db
    .collection('users').doc(userId)
  const commentRef = this.props.document
  const ticketRef = this.props.boardRef
    .collection('comments').doc(commentRef.id)
  ticketRef.update({
    userRef
  })
}

function unassignMe(){
  const commentRef = this.props.document
  const ticketRef = this.props.boardRef
    .collection('comments').doc(commentRef.id)
  ticketRef.update({
    userRef: false
  })
}

class Ticket extends Document {
  render() {
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false
    if(this.state === null ) return null
    let color = this.props.level % 2 ? styles.odd : styles.even
    return (
      <Draggable
        isDragDisabled={isMobile || !loggedIn || !isMember}
        draggableId={`${this.props.document.parent.parent.id}|${this.props.document.id}`}
        index={this.props.index}
      >
        {(provided, snapshot) => (
          <div
            className={`
              ${styles.root}
              ${snapshot.isDragging ? styles.dragging : ''}
            `}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <Response
              className={`
                ${styles.comment}
                ${color}
              `}
              areLinks={this.props.areLinks}
              boardRef={this.props.document.parent.parent}
              userRef={this.state.userRef}
              document={this.state.commentRef}>

              { loggedIn && 
                isMember && 
                <button onClick={_deleteTicket}>
                    Remove
                </button>
              }
              { loggedIn && 
                isMember && (
                  !this.state.userRef
                  ||
                  this.state.userRef &&
                  this.state.userRef.id !== this.props.user.uid
                )  &&
                <button onClick={assignMe}>
                    Assign to me
                </button>
              }
              { loggedIn &&
                isMember && 
                this.state.userRef && 
                this.state.userRef.id === this.props.user.uid &&
                <button onClick={unassignMe}>
                    Unassign me
                </button>
              }

            </Response>
            {provided.placeholder}
          </div>
        )}
      </Draggable>
    )
  }
}

export default connect(state => state)(Ticket)