import React, { Component } from 'react'
import styles from './styles.scss'
import { Document } from 'react-firestore'
import { Comment } from 'organisms'

export class CommentParents extends Document {
  constructor(props){
    super(props)
    this.state = {
      parentRef: false
    }
  }
  
  render() {
    if(!this.state.parentRef) {
      return null;
    }   
    return (
      <div>
        <CommentParents setBoard={this.props.setBoard} document={this.state.parentRef}/>
        <Comment showChildren={false} document={this.state.parentRef} />
      </div>
    )
  }
}