import Board from './board'
import Comment from './comment'
import Comments from './comments'
import Question from './question' // with router
import Questions from './questions'
import Responses from './responses' // with router
import Ticket from './ticket'
import { CommentParents } from './commentParents'
export {
    Board,
    Comment,
    Comments,
    Question,
    Questions,
    Responses,
    Ticket,
    CommentParents
}