import React, { Component } from 'react'
import Question from '../question'
import { CollectionRef } from 'react-firestore'

export default class Questions extends CollectionRef {
  constructor(props){
    super(props);
    this.state = {
      documents: []
    }
  }
  render() {
    return (
      <div>
        {this.state.documents.map(question => {
          return (
            <Question key={question.id} level="0" document={question} showChildren={false}/>
          )
        })}
      </div>
    )
  }
}