import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Response } from 'molecules'
import { Responses } from 'organisms'
import { checkForHash, addResponse } from '../../../methods/comments'

function deleteComment (){
  const commentRef = this.props.document
  return commentRef.delete()
}

class Comment extends Component {
  async addResponse(parentRef, data) {
    const tags = checkForHash(data).map(tag => tag[1]);
    const commentsRef = this.props.document.parent
    const userRef = this.props.db
        .collection('users').doc(this.props.user.uid)
    const commentRef = await addResponse(commentsRef, userRef, parentRef, data, tags)
    const responsesRef = this.responses.getWrappedInstance()
    const documents = [commentRef, ...responsesRef.state.documents]
    responsesRef.setState({ documents })
    return commentRef
  }
  render() {
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false
    let color = this.props.level % 2 ? styles.odd : styles.even;
    return (
      <div className={`${styles.root} ${this.props.className} ${color}`}>
        <div className={styles.comment}>
          <Response
            backlogRef={this.props.backlogRef}
            showImages={this.props.showImages}
            document={this.props.document}
            addResponse={this.addResponse.bind(this)}
            areLinks={this.props.areLinks}
          >
            {loggedIn && isMember && 
              <button onClick={deleteComment}>
                Delete
              </button> 
            }

            {this.props.children}
          </Response>
        </div>
        {this.props.showChildren !== false ? (
          <div className={styles.responses}>
            <Responses
              backlogRef={this.props.backlogRef}
              showImages={this.props.showImages}
              ref={responsesRef => { this.responses = responsesRef }}
              areLinks={this.props.areLinks}
              level={this.props.level}
              collection={this.props.document.collection('responses')}>
              {this.props.children}
            </Responses>
          </div>
        ) : (
            <div>

            </div>
          )}
      </div>
    )
  }
}

export default connect(state => state)(Comment)