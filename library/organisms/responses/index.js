import React, { Component } from 'react'
import { connect } from 'react-redux'
import Comment from '../comment'
import styles from './styles.scss'
import { CollectionRef } from 'react-firestore'
import { withRouter } from 'react-router-dom'

class Responses extends CollectionRef {
  constructor(props) {
    super(props)
    this.state = {
      documents: []
    }
  }

  render() {
    let clientId = this.props.collection.parent.parent.parent.id; 
    return (
      <div className={styles.responses}>
        {this.state.documents.map(doc => {
          return <Comment 
            backlogRef={this.props.backlogRef}
            level={parseInt(this.props.level) + 1}
            areLinks={this.props.areLinks}
            showImages={this.props.showImages}
            document={this.props.db.collection('clients').doc(clientId).collection('comments').doc(doc.id)} 
            key={doc.id}>
              {this.props.children}
            </Comment>
        })}
      </div>
    )
  }
}

export default connect(state => state, null, null, { withRef: true })(Responses)