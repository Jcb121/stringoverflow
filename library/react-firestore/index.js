import { Document } from './document'
import { CollectionId, CollectionRef } from './collections'

export {
  Document,
  CollectionId,
  CollectionRef
}