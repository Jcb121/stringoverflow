# react-firestore-base

Why use this? I like to keep things simple and I think this is the simplest react implementation.
Firestore stores data in either a collection or a document, so the base classes reflect that.


## Document
For a document, you want the data. the base class will add the data from the cloud into the components state.

## Collections
for a collection, you may want 1 of 2 things:
* a reference to the documents, to use in the above `Document`
* a collection of the document IDs, this is useful if your document names aren't arbitrary.

# Usage

## Setting up Firestore  
this library does not setup firebase for you,
you need to define it yourself, inline with the google docs.
I would recommend setting it up in your root react file and add it to our apps context.
that way child elements will have access to it.
```javascript
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import firebase from 'firebase'
import 'firebase/firestore'
import { firebaseConfig } from './config'
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore();
class App extends Component {
  getChildContext() {
    return {
      db
    }
  }
  render(){
    return (
      <div>your app</div>
    )
  }
}

App.childContextTypes = {
  db: PropTypes.object
}
```   
## Document
### Defining the component
Here we are defining that our component will extend the Document class. This keeps our class very clean. The only thing to note is that you have to define a default state but this is standard React.
```javascript
  import { Document } from 'react-firestore'
  import React from 'react'
  
  export default class MyDocument extends Document {
    constructor(){
      super()
      this.state = {
        data: ''
      }
    }
    render(){
      return(
        <p>{this.state.data}</p>
      )
    }
  }
```
### Using the component
```javascript
  import MyDocument from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';

  class Wrapper extends Component {
    render(){
      const documentRef = this.context.db.collection('x').doc('y')
      return(
        <MyDocument document={documentRef} />
      )
    }
  }

  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
## CollectionRef
### Defining the component
```javascript
  import { CollectionRef } from 'react-firestore'
  import React from 'react'
  
  export default class MyCollectionRef extends CollectionRef {
    render(){
      return(
        {this.state.documents.map(documentRef =>
          <MyDocument document={documentRef} />
        )}
      )
    }
  }
```
### Using the component
```javascript
  import MyCollectionRef from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';

  class Wrapper extends Component {
    render(){
      const collectionRef = this.context.db.collection('x')
      return(
        <MyCollectionRef collection={collectionRef} />
      )
    }
  }

  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
## CollectionId
### Defining the component
```javascript
  import { CollectionId } from 'react-firestore'
  import React from 'react'
  export default class MyCollectionId extends CollectionId {
    render(){
      return(
        {this.state.documents.map(documentId =>
          <div>{documentId}</div>
        )}
      )
    }
  }
```
### Using the component
```javascript
  import MyCollectionId from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';
  class Wrapper extends Component {
    render(){
      const documentRef = this.context.db.collection('x')
      return(
        <MyCollectionId document={documentRef} />
      )
    }
  }
  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
## collections
### WHERE
```javascript
  import MyCollectionRef from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';
  class Wrapper extends Component {
    render(){
      const collectionRef = this.context.db.collection('x')
      return(
        <MyCollectionRef 
          where={['public', '==', true]}
          collection={collectionRef} 
        />
      )
    }
  }
  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
### ORDERBY
```javascript
  import MyCollectionRef from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';
  class Wrapper extends Component {
    render(){
      const collectionRef = this.context.db.collection('x')
      return(
        <MyCollectionRef 
          orderBy={["createdAt", "desc"]}
          collection={collectionRef} 
        />
      )
    }
  }
  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
### LIMIT
```javascript
  import MyCollectionRef from './index'
  import React, { Component } from 'react'
  import PropTypes from 'prop-types';
  class Wrapper extends Component {
    render(){
      const collectionRef = this.context.db.collection('x')
      return(
        <MyCollectionRef 
          limit={10}
          collection={collectionRef} 
        />
      )
    }
  }
  Wrapper.contextTypes = {
    db: PropTypes.object
  };
```
## documents
### updating a document
```javascript
  import { Document } from 'react-firestore'
  import React from 'react'
  
  export default class MyDocument extends Document {
    constructor(){
      super()
      this.state = {
        data: ''
      }
    }
    handleUpdate(){
      this.props.document.update({
        foo: true,
        bar: false
      })
    }
    render(){
      return(
        <div>
          {this.state.data}
          <button onClick={this.handleUpdate.bind(this)}>update</button>
        </div>
      )
    }
  }
```
### deleting a document
```javascript
  import { Document } from 'react-firestore'
  import React from 'react'
  
  export default class MyDocument extends Document {
    constructor(){
      super()
      this.state = {
        data: ''
      }
    }
    handleDelete(){
      this.props.document.delete()
    }
    render(){
      return(
        <div>
          {this.state.data}
          <button onClick={this.handleDelete.bind(this)}>delete</button>
        </div>
      )
    }
  }
```
