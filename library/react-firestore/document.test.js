import React from 'react'
import { configure,  shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import { Document } from './document'
import { MockFireBaseRef } from './utils'

const mockData = [
  {
    exists: true,
    data: function () {
      return { data: '123' };
    }
  },
  {
    exists: true,
    data: function () {
      return { data: '456' };
    }
  },
  {
    exists: false,
  }
]

test('Document component', () => {
  const ref = new MockFireBaseRef()
  const ref2 = new MockFireBaseRef()

  const wrapper = shallow((
    <Document document={ref} />
  ))
  expect(wrapper.html()).toMatchSnapshot()

  ref.snapshotFunc(mockData[0])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()

  wrapper.setProps({document: ref2})
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()

  ref2.snapshotFunc(mockData[1])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()
})