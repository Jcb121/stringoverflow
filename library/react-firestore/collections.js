import React, { Component } from 'react'
import PropTypes from 'prop-types';

import { buildCollectionQuery } from './utils'

export class CollectionRef extends Component {
  constructor() {
    super();
    this.state = { documents: [], lastVisible: false }
    this.loadNext = this.loadNext.bind(this)
  }
  subscribe(props, next) {
    if (this.unsubscribe) this.unsubscribe();
    let ref = buildCollectionQuery.call(this, props, next);
    this.unsubscribe = ref.onSnapshot(response => {
      let documents = response.docs.map(doc => doc.ref);
      let lastVisible = response.docs[response.docs.length - 1];
      return this.setState({ documents, lastVisible })
    }, err => {
      console.warn('Error getting Collection', this.props.collection.path, this, err)
    })
  }
  loadNext() {
    this.subscribe(this.props, true)
  }
  componentWillMount() {
    this.subscribe(this.props)
  }
  componentWillReceiveProps(props) {
    this.subscribe(props)
  }
  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
    this.unsubscribe = false;
  }
  render() {
    return (
      <p>{JSON.stringify(this.state.documents)}</p>
    )
  }
}

CollectionRef.propTypes = {
  collection: PropTypes.any.isRequired
}

export class CollectionId extends Component {
  constructor() {
    super();
    this.state = { documents: [], lastVisible: false }
  }
  subscribe(props, next) {
    if (this.unsubscribe) this.unsubscribe();
    let ref = buildCollectionQuery.call(this, props, next);
    this.unsubscribe = ref.onSnapshot(response => {
      let documents = response.docs.map(doc => doc.id);
      this.setState({ documents })
    }, err => {
      console.warn('Error getting Collection', this.props.collection.path, this, err)
    })
  }
  loadNext() {
    this.subscribe(this.props, true)
  }
  componentWillMount() {
    this.subscribe(this.props)
  }
  componentWillReceiveProps(props) {
    this.subscribe(props)
  }
  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
    this.unsubscribe = false;
  }
  render() {
    return (
      <p>{JSON.stringify(this.state.documents)}</p>
    )
  }
}

CollectionId.propTypes = {
  collection: PropTypes.any.isRequired
}