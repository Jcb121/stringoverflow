export class MockFireBaseRef {
  onSnapshot(func) {
    this.snapshotFunc = func;
  }
}

export function buildCollectionQuery(props, next) {
  let ref = props.collection
  if (props.where) {
    let [val1, operator, val2] = props.where
    ref = ref.where(val1, operator, val2)
  }
  if (props.orderBy) {

    if(Array.isArray(props.orderBy)){
      let [valueName, direction] = props.orderBy
      ref = ref.orderBy(valueName, direction)
    } else {
      ref = ref.orderBy(props.orderBy)
    }

    if (next) {
      ref = ref.startAfter(this.state.lastVisible)
    }
  }
  if (props.limit) {
    ref = ref.limit(props.limit)
  }
  return ref;
}