import React from 'react'
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import {
  CollectionRef,
  CollectionId,
} from './collections'
import { MockFireBaseRef } from './utils'


const mockData = [
  {
    docs: [{
      id: '123',
      ref: '123'
    }]
  },
  {
    docs: [{
      id: '456',
      ref: '456'
    }]
  },
]

test('Collection Ref', () => {
  const ref = new MockFireBaseRef()
  const ref2 = new MockFireBaseRef()

  const wrapper = shallow((
    <CollectionRef collection={ref} />
  ))
  expect(wrapper.html()).toMatchSnapshot()

  ref.snapshotFunc(mockData[0])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()

  wrapper.setProps({ collection: ref2 })
  expect(wrapper.html()).toMatchSnapshot()

  ref2.snapshotFunc(mockData[1])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()
})

test('Collection Id', () => {
  const ref = new MockFireBaseRef()
  const ref2 = new MockFireBaseRef()

  const wrapper = shallow((
    <CollectionId collection={ref} />
  ))
  expect(wrapper.html()).toMatchSnapshot()

  ref.snapshotFunc(mockData[0])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()

  wrapper.setProps({ collection: ref2 })
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()

  ref2.snapshotFunc(mockData[1])
  wrapper.update()
  expect(wrapper.html()).toMatchSnapshot()
})