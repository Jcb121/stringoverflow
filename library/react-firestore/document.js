import React, { Component } from 'react'
import PropTypes from 'prop-types';
 
export class Document extends Component {
  subscribe(props) {
    this.unsubscribe = props.document.onSnapshot(response => {
      if (response.exists) {
        this.setState(response.data())
      }
    }, err => {
      console.warn('Error getting Doc', this.props.document.path, this, err)
    })
  }
  componentWillMount() {
    this.subscribe(this.props)
  }
  componentWillReceiveProps(props) {
    this.subscribe(props)
  }
  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
    this.unsubscribe = false;
  }
  render() {
    return (
      <p>{JSON.stringify(this.state)}</p>
    )
  }
}

Document.propTypes = {
  document: PropTypes.any.isRequired
}