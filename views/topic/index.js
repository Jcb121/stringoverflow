import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Topic, PostComment, PostQuestion } from 'molecules'
import { Card } from 'atoms'

class TopicView extends Component {
  render() {
    return (
      <div>
        <Card>
          <h2>{this.props.match.params.topic} - Tag</h2>
        </Card>
        {this.props.user.uid && [
          <Card key="postComment">
            <PostComment prefixTag={this.props.match.params.topic} />
          </Card>,
          <Card key="postQuestion">
            <PostQuestion prefixTag={this.props.match.params.topic} />
          </Card>
        ]}
        <Card>
          <Topic
            where={[`tag_${this.props.match.params.topic}`, '==', true]}
            collection={this.props.db
              .collection('clients').doc(this.props.match.params.clientId)
              .collection('comments')} />
        </Card>
      </div>
    )
  }
}
export default connect(state => state)(TopicView)

