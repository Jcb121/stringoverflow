import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Document } from 'react-firestore'
import { Header, Footer, Card, LinkButton, Button, Avatar } from 'atoms'
import { Clients } from 'molecules'
import { leaveClient } from '../../methods/client'
import { updateAvatar } from '../../methods/user'
import Dropzone from 'react-dropzone'

class UserView extends Document {
  constructor(props) {
    super(props)
    this.state = {
      displayName: ''
    }
  }

  async onDrop(acceptedFiles) {
    const [file] = acceptedFiles
    let snapShot = await updateAvatar(this.props.storage, this.props.document.id, file)
    this.props.document.update({
      userPhoto: snapShot.downloadURL
    })
  }

  render() {
    return (
      <div className={styles.root}>
        <Header
          to="/"
          className={styles.header}
        >
          {this.props.user.uid ? (
            <LinkButton to="/logout">Logout</LinkButton>
          ) : (
              <LinkButton to="/login">Login</LinkButton>
            )}
        </Header>
        <div>
          <Card>
            <h1>{this.state.displayName}</h1>
            <Dropzone
              onDrop={this.onDrop.bind(this)}
              accept=".jpeg,.png,.jpg,.gif"
              multiple={false}
              className={styles.dropzone}
            >
              <Avatar
                document={this.props.document}
                className={styles.avatar} />
              <Button>Update Avatar</Button>
            </Dropzone>
          </Card>

          <Card>
            <h2>Your clients</h2>
            <Clients
              className={styles.client}
              requestClassName={styles.client_requests}
              collection={this.props.document.collection('clients')}>
              {(clientRef) => (
                <div className={styles.client_action}>
                  <Button onClick={() => leaveClient(clientRef, this.props.document)}>
                    Leave
                  </Button>
                </div>
              )}
            </Clients>
          </Card>
        </div>
        <Footer />
      </div>
    )
  }
}

export default connect(state => state)(UserView)
