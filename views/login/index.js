import React, { Component } from 'react'
import firebase from 'firebase'
import { withRouter } from 'react-router-dom'
import { Button, Footer } from 'atoms'
import styles from './styles.scss'

const provider = new firebase.auth.GoogleAuthProvider();

class LoginView extends Component {
  login() {
    firebase.auth().signInWithPopup(provider).then(result => {
      var token = result.credential.accessToken;
      var user = result.user;
      this.props.history.push('/')
    }).catch(function (error) {
      alert(`Login Error: ${error}`)
    });
  }
  render() {
    return (
      <div className={styles.root}>
        <div>
          Header
        </div>

        <div className={styles.wrapper}>
          <h1 className={styles.title}>
            <div className={styles.small}>Welcome to</div>
            <div className={styles.main}>Overflow</div>
            </h1>
          <p>Please login to continue</p>
          <Button
            onClick={this.login.bind(this)}
          >
            Login with Google
          </Button>



        </div>

        <Footer />

      </div>
    )
  }
}

export default withRouter(LoginView)