import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { PostQuestion } from 'molecules'
import { Questions } from 'organisms'

class QuestionsView extends Component {
  constructor(props){
    super()
    this.questionsRef = props.db.collection('clients').doc(this.props.match.params.clientId).collection('comments').where('question', '==', true)    
  }
  render(){
    return (
      <div>
        <PostQuestion />
        <Questions collectionRef={this.questionsRef}/>
      </div>
    )
  }
}
export default connect(state => state)(QuestionsView)
