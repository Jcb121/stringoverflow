import React, { Component } from 'react'
import styles from './styles.scss'
import { RequestClient, CreateClient, Clients } from 'molecules'
import { Header, Footer, LinkButton, Card } from 'atoms'
import { isMobile } from 'react-device-detect'
import { connect } from 'react-redux'

class ClientsView extends Component {
  render() {
    return (
      <div className={`
        ${styles.root}
        ${isMobile ? styles.mobile : styles.desktop}
      `}>
        <Header 
          to="/"
          className={styles.header}
        >

        {this.props.user.uid && 
          <LinkButton to={`/users/${this.props.user.uid}`}>
            Account
          </LinkButton>
        }

        {this.props.user.uid ? (
          <LinkButton to="/logout">Logout</LinkButton>
        ) : (
          <LinkButton to="/login">Login</LinkButton>
        )}
        </Header>
        
        <div>
          <div className={styles.wrapper}>
            {this.props.user.uid && 
              <Card>
                <h2>Your clients</h2>
                <Clients collection={this.props.db.collection('users').doc(this.props.user.uid).collection('clients')} />
              </Card>
            }
          
            <Card className={styles.section}>
              <h2>Public Clients</h2>
              <Clients
                collection={this.props.db.collection('clients')}
                where={['public', '==', true]}
              />
            </Card>

            {this.props.user.uid && 
              <Card>
                <h2>Create a client</h2>
                <CreateClient />
              </Card>

            }
            {this.props.user.uid && 
              <Card>
                <h2>Request access to a client</h2>
                <RequestClient />
              </Card>
            }
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default connect(state => state)(ClientsView)