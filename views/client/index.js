import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route } from 'react-router'
import styles from './styles.scss'
import QuestionsView from '../questions'
import QuestionView from '../question'
import CommentView from '../commentView'
import TopicView from '../topic'
import HomeView from '../home'
import BoardView from '../board'

import { Document } from 'react-firestore'
import { UserHeader, Search } from 'molecules'
import { Header, Footer, ClientSelect, Link, LinkButton } from 'atoms'

class ClientView extends Document {
  render() {
    return (
      <div className={styles.root}>
        <Header
          to={`/clients/${this.props.match.params.clientId}`}
          className={styles.header}
        >
          <Search
            collection={this.props.db.collection('clients').doc(this.props.match.params.clientId).collection('topics')}
          />
          <div className={styles.headerWrapper}>
            {/* {this.props.user.uid &&
              <ClientSelect
                collection={this.props.db.collection('users').doc(this.props.user.uid).collection('clients')}
              />
            } */}

            <LinkButton to="/">
              Clients
            </LinkButton>

            {this.props.user.uid &&
              <LinkButton to={`/users/${this.props.user.uid}`}>
                Account
              </LinkButton>
            }

            {this.props.user.uid &&
              <LinkButton to="/logout">
                Logout
              </LinkButton>
            }

            {/* {this.props.user.uid && 
              <Link 
                to={`/users/${this.props.user.uid}`}
                className={styles.action}
              >
                Account
              </Link>
            } */}

            {!this.props.user.uid &&
              <LinkButton to="/login">Login</LinkButton>
            }

          </div>

        </Header>
        <Route exact path='/clients/:clientId' render={routeProps => {
          return <HomeView
            {...routeProps}
            document={this.props.document}
          />
        }} />

        <Route exact path='/clients/:clientId/questions' component={QuestionsView} />

        <Route path='/clients/:clientId/questions/:commentId' render={routeProps => {
          const commentRef = this.props.db
            .collection('clients')
            .doc(routeProps.match.params.clientId)
            .collection('comments')
            .doc(routeProps.match.params.commentId)
          return <CommentView document={commentRef} {...routeProps} />
        }} />

        <Route path='/clients/:clientId/comments/:commentId' render={routeProps => {
          const commentRef = this.props.db
            .collection('clients')
            .doc(routeProps.match.params.clientId)
            .collection('comments')
            .doc(routeProps.match.params.commentId)
          return <CommentView document={commentRef} {...routeProps} />
        }} />

        <Route path='/clients/:clientId/tickets/:commentId' render={routeProps => {
          const commentRef = this.props.db
            .collection('clients')
            .doc(routeProps.match.params.clientId)
            .collection('comments')
            .doc(routeProps.match.params.commentId)
          return <CommentView document={commentRef} {...routeProps} />
        }} />

        <Route path='/clients/:clientId/topics/:topic' component={TopicView} />

        <Route path='/clients/:clientId/boards/:boardId' component={BoardView} />
        {/* <Route path='/topics' component={TopicsView} />  */}
        <Footer />
      </div>
    )
  }
}

export default connect(state => state)(ClientView)
