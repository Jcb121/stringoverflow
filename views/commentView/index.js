import React from 'react'
import { connect } from 'react-redux'

import styles from './styles.scss'
import { DragDropContext } from 'react-beautiful-dnd'
import { Comment, CommentParents, Board } from 'organisms'
import { Card } from 'atoms'
import { dragEnd, makeTicket } from '../../methods/board'
import { Document } from 'react-firestore'

function makeTask(){
  const clientRef = this.props.document.parent.parent
  const parentRef = this.state.parentRef
  const boardRef = clientRef.collection('boards').doc(parentRef.id)
  makeTicket(boardRef, this.props.document, true)    
}

class CommentView extends Document {
  constructor(props){
    super(props)
    this.state = {
      parentRef: false,
      boardRef: false
    } 
  }
  render(){
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false
    return (
      <div className={styles.root}>
        {this.state.parentRef && 
          <Card>
            <p>Parents:</p>
            <CommentParents
              document={this.props.document} 
            />
          </Card>
        }
        <Card>
          <Comment 
            showChildren={true} 
            showImages={true}
            level="0" 
            document={this.props.document} 
          >
            {loggedIn && isMember && this.state.boardRef && 
              <button onClick={makeTask}>Make Task</button>
            }
          </Comment>
        </Card>
        {this.state.boardRef &&
          <DragDropContext onDragEnd={dragEnd.bind(this)}>
            <Card>
              <Board
                ref={boardRef => this[this.state.boardRef.id] = boardRef} 
                document={this.state.boardRef}
                areLinks={false}/>
            </Card>
          </DragDropContext>
        }
      </div>
    )
  }
}

export default connect(state => state)(CommentView)
