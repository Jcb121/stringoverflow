import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DragDropContext } from 'react-beautiful-dnd'
import styles from './styles.scss'
import { Card } from 'atoms'
import { Board } from 'organisms'
import { dragEnd } from '../../methods/board'

class BoardView extends Component {
  render() {
    let boardId = this.props.match.params.boardId;
    let rootDoc = this.props.db.collection('clients').doc(this.props.match.params.clientId)
    let boardRef = rootDoc.collection('boards').doc(boardId)

    return (
      <DragDropContext onDragEnd={dragEnd.bind(this)}>
        <Card className={styles.root}>
          <Board ref={boardRef => this[boardId] = boardRef} document={boardRef} />
        </Card>
      </DragDropContext>
    )
  }
}
export default connect(state => state)(BoardView)
