import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.scss'
import { Comment } from 'organisms'

class QuestionView extends Component {
  render(){
    let question = this.props.db
      .collection('clients')
      .doc(this.props.match.params.clientId)
      .collection('comments')
      .doc(this.props.match.params.questionId)
    
    return (
      <div>
        <h2>Question</h2>
        <Comment level="0" document={question} showChildren={true}/>
      </div>
    )
  }
}

export default connect(state => state)(QuestionView)

