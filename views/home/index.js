import React from 'react'
import { connect } from 'react-redux'
import { Link, Card, ElementDropper, Button, Title } from 'atoms'
import {
  PostTopic,
  PostComment,
  PostQuestion,
  Topics,
  FormRow,
  Tasks,
  CTA
} from 'molecules'
import { Questions, Comments, Board } from 'organisms'
import styles from './styles.scss'
import { DragDropContext } from 'react-beautiful-dnd'
import { isBrowser, isMobile } from 'react-device-detect'
import { Document } from 'react-firestore'
import { dragEnd, createBoard, addToBoard } from '../../methods/board'

function moveToBacklog() {
  const commentRef = this.props.document
  const boardRef = this.props.backlogRef
  makeTicket(boardRef, commentRef)
}

class HomeView extends Document {
  constructor() {
    super();
    this.state = {
      boards_order: [],
      newBoard: false,
      state: ''
    }
  }
  makePublic(_public = true) {
    this.props.document.update({ 'public': _public })
  }
  handleBoardChange(e) {
    this.setState({ state: e.target.value })
  }
  async addNewBoard() {
    const clientRef = this.props.document;
    const boardName = this.state.state
    const columns = ['todo']
    let boardref = await createBoard(clientRef, boardName, columns)
    this.setState({ newBoard: false, state: '' })
  }
  requestToJoin(){
    // implement soon
  }
  render() {
    const rootDoc = this.props.db.collection('clients').doc(this.props.match.params.clientId)
    const commentsRef = rootDoc.collection('comments')
    const questionsRef = rootDoc.collection('comments')
    const topicsRef = rootDoc.collection('topics')
    const isMember = this.props.user.clients.includes('overflow')
    const loggedIn = this.props.user.uid !== false

    return (
      <div className={`
        ${styles.root} 
        ${isBrowser ? styles.browser : styles.mobile}
      `}>
        <div className={styles.preBoard}>
          <Topics className={styles.topics} collection={topicsRef} />
          <div className={styles.preBoard_cta}>
            { loggedIn && isMember &&
              <div className={styles.addBoard}>
                <Button onClick={() => this.setState({ newBoard: true })}>New Board</Button>
                {this.state.newBoard &&
                  <Card className={styles.addBoard_card}>
                    <FormRow
                      value={this.state.state}
                      label="new board name:"
                      type="text"
                      onChange={this.handleBoardChange.bind(this)}
                    />
                    <CTA
                      disabled={this.state.loading}
                      onAgree={this.addNewBoard.bind(this)}
                      onCancel={() => { this.setState({ newBoard: false, state: '' }) }}
                    >
                      Create
                  </CTA>
                  </Card>
                }
              </div>
            }
            { loggedIn && !isMember &&
              <Button onClick={this.requestToJoin.bind(this)}>Request To Join</Button>
            }
            { loggedIn && isMember && this.state.public &&
              <Button onClick={this.makePublic.bind(this, false)}>Make Private</Button>
            } 
            { loggedIn && isMember && !this.state.public &&
              <Button onClick={this.makePublic.bind(this)}>Make Public</Button>
            }
          </div>
        </div>

        {isMobile &&
          <Card className={styles.boards}>
            <h2 className={styles.title}>Your Boards</h2>
            <ul className={styles.boards_wrapper}>
              {this.state.boards_order.map(boardId => {
                const commentRef = commentsRef.doc(boardId)
                return (
                  <li key={boardId} className={styles.boards_board}>
                    <Link
                      className={styles.boards_link}
                      to={`/clients/${this.props.match.params.clientId}/boards/${boardId}`}>
                      <Title type="div" document={commentRef} after=" - Board" />
                    </Link>
                  </li>
                )
              }
              )}
            </ul>
          </Card>
        }

        {isBrowser &&
          <DragDropContext 
            onDragEnd={dragEnd.bind(this)}>
            {this.state.boards_order.map(boardId => {
              let boardRef = rootDoc.collection('boards').doc(boardId)
              return (
                <Card key={`board|${boardId}`} className={styles.board}>
                  <Board
                    areLinks={true}
                    ref={boardRef => this[boardId] = boardRef}
                    document={boardRef} />
                </Card>
              )
            })}
          </DragDropContext>
        }
        
        {/* <Card>
          <h2 className={styles.title}>Your tasks</h2>
          <Tasks
            collection={rootDoc.collection('boards')}
          />
        </Card> */}

        <Card className={styles.comments}>
          {loggedIn && isMember &&
            <PostComment
              backlogRef={this.state.backlogRef}
            />
          }
          <h2 className={styles.title}>Comments</h2>
          <Comments
            limit={10}
            orderBy={["createdAt", "desc"]}
            showChildren={false}
            backlogRef={this.state.backlogRef}
            showImages={true}
            collection={commentsRef} >
            {loggedIn && isMember &&
              <button
              onClick={moveToBacklog}>
                Add to backlog
              </button>
            }
          </Comments>
        </Card>

        <Card className={styles.questions}>
          {loggedIn && isMember &&
            <PostQuestion />
          }
          <h2 className={styles.title}>Questions</h2>
          <Questions
            where={['question', '==', true]}
            collection={questionsRef} />
        </Card>

      </div>
    )
  }
}

export default connect(state => state)(HomeView)

