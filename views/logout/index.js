import { withRouter } from 'react-router-dom'
import React, { Component} from 'react'
import firebase from 'firebase'


export default class LogoutView extends Component {  
  
  componentWillMount(){
    firebase.auth().signOut().then(() => {
      console.log('logout')
    })
  }

  render(){
    return (
      <h2>You have been logged out</h2>
    )
  }
}

