import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from '../app'
import { Provider } from 'react-redux'
import reducers from '../reducers'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

const root = document.getElementById('root')

const initialState = window.__APP_INITIAL_STATE__
delete window.__APP_INITIAL_STATE__
const store = createStore(reducers, initialState, applyMiddleware(
  thunkMiddleware
))

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
  , root)