// function boardToBoard() {
// }

// function boarrdInternalMove() {
// }

export function dragEnd({ source, draggableId, destination }) {
  const clientId = this.props.match.params.clientId
  if (this.props.user.uid == false) return
  if (!destination) return
  // console.log(source, draggableId, 'dest:', destination)
  // console.log(this)
  let targetIndex = destination.index

  let [sourceBoard, sourceColumn] = source.droppableId.split('|')
  let [targetBoard, targetColumn] = destination.droppableId.split('|')
  let [currentBoard, commentId] = draggableId.split('|')
  let boardsRef = this.props.db.collection('clients').doc(clientId).collection('boards')
  let docRef = boardsRef.doc(targetBoard).collection('comments').doc(commentId)

  // currentBoard === undefined. this is the issue.

  if (currentBoard !== targetBoard) {
    //if it has moved boards, but the original comment is still in /commments!
    let sourceDocuments = this[sourceBoard].state[`${sourceColumn}_order`].filter(id => id !== commentId)
    let targetOrderArray = this[targetBoard].state[`${targetColumn}_order`]
    targetOrderArray.splice(targetIndex, 0, commentId)
    this[sourceBoard].setState({ [`${sourceColumn}_order`]: sourceDocuments })
    this[targetBoard].setState({ [`${targetColumn}_order`]: targetOrderArray })


    const boardRef = boardsRef.doc(targetBoard)
    const sourceBoardRef = boardsRef.doc(sourceBoard)
    const commentRef = this.props.db.collection('clients').doc(clientId).collection('comments').doc(commentId)
    const rootComment = true
    // const userRef // this should be equal to the current value! but where do I get this form. handle server side?

    docRef.set({
      boardRef, // -- arg
      commentRef, // -- arg
      rootComment, // -- arg - this creates a new board for the ticket. what if there already is one?
      sourceBoardRef, // -- arg - this deletes the ticket from the source board
      // userRef -- meta // how do I get this from the orignal ticket? 
      targetColumn,
      targetIndex
    })
  } else {
    //if it has moved columns, but still on the same board
    if (sourceColumn !== targetColumn) {
      let sourceOrderArray = this[targetBoard].state[`${sourceColumn}_order`].filter(id => id !== commentId)
      let targetOrderArray = this[targetBoard].state[`${targetColumn}_order`]
      targetOrderArray.splice(targetIndex, 0, commentId)
      // this is the issue. component is unmounted. why?
      this[targetBoard].setState({
        [`${sourceColumn}_order`]: sourceOrderArray,
        [`${targetColumn}_order`]: targetOrderArray
      })
    } else {
      // the the orering has changed in the same column
      let targetOrderArray = this[targetBoard].state[`${targetColumn}_order`].filter(id => id !== commentId)
      targetOrderArray.splice(targetIndex, 0, commentId)
      this[targetBoard].setState({
        [`${targetColumn}_order`]: targetOrderArray
      })
    }
    docRef.update({
      targetColumn,
      targetIndex
    })
  }
}

export function createBoard(clientRef, boardName, columns, targetIndex = false) {
  return new Promise((resolve, reject) => {
    clientRef.collection('comments').add({
      data: boardName,
      rootComment: true,
      tags: ['boards']
    }).then(commentRef => {
      const boardRef = clientRef.collection('boards').doc(commentRef.id)
      boardRef.set({
        rootBoard: true,
        targetIndex,
        columns,
        commentRef
      }).then(() => {
        if (boardName === 'backlog') {
          clientRef.update({
            backlogRef: boardRef
          })
        }
        commentRef.update({ boardRef })
        resolve(boardRef)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    }).catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

export function deleteTicket(boardRef, commentRef) {
  return boardRef.collection('comments').doc(commentRef.id).delete()
}

export function makeTicket(
  boardRef,
  commentRef,
  rootComment = false,
  targetIndex = false,
  sourceBoardRef = false
) {
  boardRef.collection('comments').doc(commentRef.id).set({
    boardRef,
    commentRef,
    rootComment,
    targetIndex,
    sourceBoardRef,
    targetColumn: 'todo',
  })
}

export function updateColumns(boardRef, columns) {
  return boardRef.update({ columns })
}