export function setupUser(
  userRef,
  displayName,
  emailVerified) {
  const userDetails = {
    displayName: displayName,
    emailVerified: emailVerified
  }
  return userRef.get().then(doc => {
    if (doc.exists) {
      userRef.update(userDetails)
    } else {
      userRef.set(userDetails)
    }
  })
}

export function updateAvatar(storageContext, userId,  file) {
  return storageContext.ref(`users/${userId}`)
    .put(file)
}