export function approveUser(clientRef, userRef) {
  // add the user to the clients user.
  // the backend should delete the rest.
  clientRef.collection('users').doc(userRef.id).set({
    userRef
  })
}

export function createClient(clientsRef, name, userId) {
  return clientsRef.doc(name).set({
    userId
  })
}

export function postTopic(topicsRef, topic) {
  return topicsRef
    .doc(topic)
    .set({ topic })
}

export function requestClient(userRef, clientName) {
  return userRef.collection('requests').doc(clientName)
    .set({
      temp: 'temp'
    })
}

export function leaveClient(clientRef) {
  // this should be at /users/{userId}/clients/{client}
  return clientRef.delete()
}