const regex = /#(\w+)+/g

export function checkForHash(comment){
  comment = comment.trim()
  var m
  var result = []
  while ((m = regex.exec(comment)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++
    }
    result.push(m)
  }
  return result
}

export function postComment(commentsRef, userRef, data, tags){
  return commentsRef.add({
    data,
    tags,
    userRef,
    rootComment: true,
    question: false,
  })
}

export function attachImage(commentRef, urls){
  return commentRef.update({
    images: urls
  })
}

export function uploadImage(storageContext, clientId, commentId, file){
  return storageContext.ref(`${clientId}/${commentId}/${file.name}`)
    .put(file)
}

export function postQuestion(commentsRef, userRef, data, tags) {
  return commentsRef.add({
    data,
    userRef,
    tags,
    question: true,
    rootComment: false
  })
}

export function addResponse(commentsRef, userRef, parentRef, data, tags) {
  return commentsRef.add({
    userRef,
    data,
    tags,
    parentRef,
    question: false,
    rootComment: false
  })
}

export function addCommentToBacklog() {

}

export function updateCommentText(commentRef, data){
  return commentRef.update(data)
}

export function solveQuestion(commentRef){
  commentRef.update({ resolved: true}) 
}