var fs = require('fs')
var path = require('path')
const WebpackShellPlugin = require('webpack-shell-plugin')

module.exports = {

  entry: path.resolve(__dirname, 'server/index.js'),

  output: {
    filename: './dist/server.bundle.js'
  },

  target: 'node',

  plugins: [
    new WebpackShellPlugin({ onBuildStart: ['echo "Webpack Start"'], onBuildEnd: ['npm run server:start'] })
  ],

  // keep node_module paths out of the bundle
  externals: fs.readdirSync(path.resolve(__dirname, 'node_modules')).concat([
    'react-dom/server', 'react/addons',
  ]).reduce(function (ext, mod) {
    ext[mod] = 'commonjs ' + mod
    return ext
  }, {}),

  node: {
    __filename: true,
    __dirname: true
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=es2015&presets[]=react'
      },
      {
        test: /\.scss$/,
        loader: [
          'css-loader/locals?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
          'sass-loader'
        ]
      }
    ]
  }

}