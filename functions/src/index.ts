import * as functions from 'firebase-functions';
const fsf = functions.firestore.document 
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//

/*
 * handle all meta in this topic.
 */

function createBoard(clientRef, boardName, columns, targetIndex = false) {
  return new Promise((resolve, reject) => {
    clientRef.collection('comments').add({
      data: boardName,
      rootComment: true,
      tags: ['boards']
    }).then(commentRef => { 
      const boardRef = clientRef.collection('boards').doc(commentRef.id)
      boardRef.set({
        rootBoard: true,
        targetIndex,
        columns,
        commentRef
      }).then(() => {  
        if(boardName === 'backlog'){
          clientRef.update({
            backlogRef: boardRef
          })
        }
        commentRef.update({ boardRef })
        resolve(boardRef)
      }).catch(error => {
        console.log(error)
        reject(error)  
      })
    }).catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

exports.comment = {
  commentDelete: fsf('clients/{clientName}/comments/{commentId}').onDelete(event => {
    // find the board the comment is listed on
    return event.data.ref.parent.parent.collection('boards').get().then(response => {
      response.docs.forEach(board => {
        board.ref.collection('comments').doc(event.params.commentId).delete();
      });
    })
  }),
  commentCreate: fsf('clients/{clientName}/comments/{commentId}').onCreate(event => {
    const commentsRef = event.data.ref.parent;
    const topicsRef = event.data.ref.parent.parent.collection('topics')
    const { parentRef, tags } = event.data.data();
    let tagObject = {}
  
    if(tags.length > 0){
      // create the tag object
      tagObject = tags.reduce((object, tag) => {
        object[`tag_${tag}`] = true;
        return object
      }, {})
      // for each tag, make sure it exists somewhere
      tags.forEach(tag => {
        topicsRef.doc(tag).set({test: 'test'}, {merge: true})
      })
    }
  
    if(parentRef){
      commentsRef.doc(parentRef.id)
        .collection('responses').doc(event.params.commentId)
        .set({ temp: 'temp'})
    }
  
    return event.data.ref.update({
      rating: 0,
      createdAt: event.timestamp,
      //createdBy: functions.auth.UserRecordMetadata.uid,
      lastEdited: event.timestamp,
      // lastEditedBy: functions.auth.user.uid,
      ...tagObject
    })
  })
}

exports.client = {
  clientCreate: fsf('clients/{clientName}').onCreate(event => {
    const { userId } = event.data.data()
    const userRef = event.data.ref.parent.parent.collection('users').doc(userId)
  
    const clientUpdatePromise = event.data.ref.update({
      admins: [ userId ],
      boards_order: []
    })
    const userRefPromise = event.data.ref.collection('users').doc(userId).set({ref: userRef})  
    const backLogPromise = createBoard(event.data.ref, 'backlog' , ['todo'])
    const defaultPromise = createBoard(event.data.ref, 'default', ['todo', 'wip', 'testing', 'done'])
  
    return Promise.all([
      clientUpdatePromise, 
      backLogPromise, 
      defaultPromise, 
      userRefPromise
    ])
  }),
  clientAddUser: fsf('clients/{clientName}/users/{userId}').onCreate(event => {
    const { clientName, userId } = event.params
    const clientRef = event.data.ref.parent.parent
    const rootRef = clientRef.parent.parent
    const userRef = rootRef.collection('users').doc(userId)

    userRef.get().then(response => {
      const { clients } = response.data()
      userRef.update({clients: [
        ...clients,
        clientName
      ]})      
    })
    return userRef
      .collection('clients').doc(clientName)
      .set({ref: clientRef})
  })
}

exports.user = {
  userRequestCreate: fsf('users/{userId}/requests/{clientName}').onCreate(event => {
    const rootRef = event.data.ref.parent.parent.parent.parent
    const userRef = event.data.ref.parent.parent
  
    const { userId, clientName } = event.params
    const clientRef = rootRef.collection('clients').doc(clientName)
  
    clientRef.get().then(response => {
      if(response.exists){
        clientRef.collection('requests').doc(userId).set({ ref: userRef })
      } else {
        console.log('request to client that didnot exist')
      }
    })
  
    
  }),
  userLeaveClient: fsf('users/{userId}/clients/{clientName}').onDelete(event => {
    // remove the user ref from the client
    const { userId, clientName } = event.params
    const userRef = event.data.ref.parent.parent
    const rootRef = userRef.parent.parentRef
    const clientRef = rootRef.collection('clients').doc(clientName)
    return clientRef.collection('users').doc(userId).delete()
  })
}

exports.ticket = {
  ticketCreate: fsf('clients/{clientName}/boards/{targetBoard}/comments/{commentId}').onCreate(event => {
    const { 
      sourceBoardRef,
      targetColumn, 
      targetIndex, 
      rootComment,
    } = event.data.data();
    const targetOrder = targetColumn + '_order'
    const boardRef = event.data.ref.parent.parent
    const { commentId } = event.params
    const clientRef = event.data.ref.parent.parent.parent.parent
    const commentRef = clientRef.collection('comments').doc(commentId)
  
    if (sourceBoardRef) {
      // if the ticket has come from another board
      
      const originalCommentRef = sourceBoardRef.collection

      sourceBoardRef
        .collection('comments').doc(commentId)
        .delete()
    } else {
      // if it's a ticket and it has not come from another board, it should have a sub board.
      // ONLY IF IT'S A ROOT COMMENT. rootcomment -> root board.
      if(rootComment){
        const subBoardRef = boardRef.parent.doc(commentId)
        subBoardRef.set({
          columns: ['todo', 'wip', 'done'],
          todo_order: [],
          wip_order: [],
          done_order: []
        }).then((ref) => {
          console.log('ref', ref)
          // add a reference to the board to the comment
          commentRef.update({
            boardRef: subBoardRef
          }) 
        })
      } else {
        commentRef.update({
          boardRef
        })
      }
    }
  
    return boardRef.get().then(response => {
      const data = response.data()
  
      let newOrder = data[targetOrder]
      if (targetIndex > -1) {
        newOrder.splice(targetIndex, 0, commentId)
      } else {
        newOrder = [...newOrder, commentId]
      }
      boardRef.update({
        [targetOrder]: newOrder
      })
    })
  }),
  ticketUpdate: fsf('clients/{clientName}/boards/{targetBoard}/comments/{commentId}').onUpdate(event => {
    const sourceOrder = event.data.previous.data().targetColumn + '_order';
    const targetOrder = event.data.data().targetColumn + '_order';
    const { targetIndex } = event.data.data()
    const board = event.data.ref.parent.parent
  
    return board.get().then(response => {
      const data = response.data()
      const { commentId } = event.params
      let newOrder = data[targetOrder].filter(id => id !== event.params.commentId)
      if (targetIndex > -1) {
        newOrder.splice(targetIndex, 0, event.params.commentId)
      } else {
        newOrder = [...newOrder, event.params.commentId]
      }
      board.update({
        [sourceOrder]: data[sourceOrder].filter(id => id !== commentId),
        [targetOrder]: newOrder
      })
    })
  }),
  ticketDelete: fsf('clients/{clientName}/boards/{targetBoard}/comments/{commentId}').onDelete(event => {
    const { targetColumn } = event.data.previous.data()
    const targetOrder = targetColumn + '_order'
    const board = event.data.ref.parent.parent
  
    return board.get().then(response => {
      const orderArray = response.data()[targetOrder].filter(id => id !== event.params.commentId)
      board.update({
        [targetOrder]: orderArray
      })
    })
  })
}

exports.board = {
  boardDelete: fsf('clients/{clientName}/boards/{targetBoard}').onDelete(event => {
    const clientRef = event.data.ref.parent.parent
    const { targetBoard } = event.params

    // delete the comment with the board title
    clientRef.collection('comments').doc(targetBoard).delete()
    
    return clientRef.get().then(response => {
      let { boards_order } = response.data()
      boards_order = boards_order.filter(boardId => boardId !== targetBoard) 
      clientRef.update({ boards_order })
    })
  }),
  boardUpdate: fsf('clients/{clientName}/boards/{targetBoard}').onUpdate(event => {
    // this is when a board is updated.
    /**
     * What does this need to do?
     * 1. create/delete indexes if none
     */
    // get the new columns
    const data = event.data.data()
    const boardRef = event.data.ref
    const previousData = event.data.previous.data();

    if(data.columns !== previousData.columns){
      const boardIndexes = data.columns.reduce((arg, column) => {
        if(typeof data[column + '_order'] === 'undefined'){
          arg[column + '_order'] = [];
        }
        return arg;
      }, {});
      if(Object.keys(boardIndexes).length !== 0 && boardIndexes.constructor === Object){
        return boardRef.update(boardIndexes)
      }
    }
    return
  }),
  boardCreate: fsf('clients/{clientName}/boards/{boardId}').onCreate(event => {
    // this is when a board is created.
    const { columns, rootBoard, targetIndex } = event.data.data()
    const { boardId } = event.params

    const boardRef = event.data.ref
    const boardIndexes = columns.reduce((arg, column) => {
      arg[column + '_order'] = [];
      return arg;
    }, {});
    // THIS TRIGGERS THE BOARD UPDATE!
    // ERROR HERE
    boardRef.update(boardIndexes)
  
    if(typeof rootBoard === 'undefined' || rootBoard === false) {
      return
      // don't add it to boards_order if it's not a rootBoard
    }
  
    const clientRef = event.data.ref.parent.parent;
    return clientRef.get().then(response => {
      let { boards_order } = response.data()
      if(typeof boards_order === 'undefined'){
        boards_order = [];
      }
      boards_order = boards_order.filter(id => id !== boardId)
      if(targetIndex > -1){
        boards_order.splice(targetIndex, 0, boardId)
      } else {
        boards_order.push(boardId)
      }
      clientRef.update({boards_order})
    })
  })
}