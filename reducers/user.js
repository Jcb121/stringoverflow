const DEFAULT_STATE = {
  uid: false,
  clients: []
}

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
  case 'SET_USER':
    return {
      ...state,
      ...action.payload
    }
  }
  return state
}