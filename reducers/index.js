import { combineReducers } from 'redux'

import db from './db'
import user from './user'
import storage from './storage'

export const reducerParts = {
  db,
  storage,
  user
}

const allReducers = combineReducers(reducerParts)

export default allReducers