const DEFAULT_STATE = false

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
  case 'SET_STORAGE':
    return action.payload
  }
  return state
}