const DEFAULT_STATE = false

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
    case 'SET_DB':
      return action.payload
  }
  return state
}