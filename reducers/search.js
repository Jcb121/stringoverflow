const DEFAULT_STATE = {
  state: '',
  results: []
}

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
  case 'UPDATE_SEARCH_STATE':
    return {
      ...state,
      state: action.payload
    }
  }
  return state
}