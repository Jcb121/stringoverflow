import React, { Component } from 'react'
import { Route } from 'react-router'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import firebase from 'firebase'
import 'firebase/firestore'
import { firebaseConfig } from './config'
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const storage = firebase.storage()

import styles from './styles.scss'

// Views
import LoginView from '../views/login'
import LogoutView from '../views/logout'
import ClientView from '../views/client'
import ClientsView from '../views/clients'
import UserView from '../views/userView'

import { setupUser } from '../methods/user'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
    props.setDb(db)
    props.setStorage(storage)
  }
  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        let userRef = db.collection('users').doc(user.uid)
        userRef.onSnapshot(Response => {
          this.setState({ loading: false })
          this.props.setUser({
            ...Response.data(),
            uid: user.uid,
            ref: userRef
          })
        })
        
        setupUser(userRef, user.displayName, user.emailVerified)
      } else {
        this.setState({ loading: false })
        // this.props.history.push('/')
      }
    })
  }

  loading(loading) {
    this.setState({ loading })
  }

  render() {
    return (
      <div className={styles.root}>
        <Route path='/users/:userId' render={routeProps => {
          const userRef = db.collection('users').doc(routeProps.match.params.userId)
          return <UserView
            {...routeProps}
            document={userRef}
          />
        }} />
        <Route path='/login' component={LoginView} />
        <Route path='/logout' component={LogoutView} />

        {this.state.loading &&
          <div className={styles.loading}>
            <div>Spinner</div>
          </div>
        }

        <Route exact path='/' render={(routeProps) => {
          return <ClientsView
            {...routeProps}
          />
        }} />
        <Route path='/clients/:clientId' render={(routeProps) => {
          let clientRef = db.collection('clients').doc(routeProps.match.params.clientId)
          return <ClientView
            {...routeProps}
            document={clientRef}
          />
        }} />
        {this.state.loading &&
          <h1>Loading</h1>
        }
      </div>
    )
  }
}

const actions = {
  setDb(payload){
    return {
      type: 'SET_DB',
      payload
    }
  },
  setStorage(payload){
    return {
      type: 'SET_STORAGE',
      payload
    }
  },
  setUser(payload){
    return {
      type: 'SET_USER',
      payload
    }
  }
}

export default withRouter(connect(null, actions)(App))