const PORT = process.env.NODE_ENV === 'production' ? 80 : 8000
import express from 'express'
import bodyParser from 'body-parser'
import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router'
import reducers from '../reducers'
import siteTemplate from './template.js'

import App from '../components'

const app = express()

app.use(bodyParser())
app.use(express.static('./dist'))

app.get('/', (req, res) => {

  const defaultState = {}
  const store = createStore(reducers, defaultState)
  const appString = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url}>
        <App />
      </StaticRouter>
    </Provider>
  )
  const initialState = store.getState()
  res.send(siteTemplate({
    body: appString,
    title: `StringOverflow`,
    initialState: JSON.stringify(initialState)
  }))
})

app.listen(PORT, function () {
  console.log(`App listening on port ${PORT}!`)
})
